Inline GApps for 32bit devices based on NikGApps Core
=====================================================

This repo is for building custom roms with built-in GApps for Android 11 on 32bit ARM devcies.

It uses NikGapps Core as the base package and ensuring all APK's provided are compatible with 32bit ARM.

Usage
-----

 - Sync this repo in your local manifest:
```
   <remote fetch="https://gitlab.com" name="gitlab"/>
   <project name="android_vendor_google_gms" path="vendor/google/gms" remote="gitlab" revision="11.0" />
```


 - Include one of the provided makefiles from your device tree or ROM source

    Core: For core GApps functionality as provided by NikGapps Core

    `include vendor/google/gms/gms_core.mk`


    Basic: All 'Core' functionality plus Contacts, Dialer, Messages, Calendar, GBoard, Clock and Wellbeing

    `include vendor/google/gms/gms_basic.mk`

    Full: Currently the same as 'Basic' but may be extended later.

    `include vendor/google/gms/gms_full.mk`

Tip
---

 - Some roms attempt to include a GApps packge if the required repo exists. It is possible to use this repo in it's place using the copyfile or linkfile directives to adapt to the include path specified by the rom source. 

```
  	<project name="android_vendor_google_gms" path="vendor/google/gms" remote="gitlab" revision="11.0" >
	    <copyfile src="gms_core.mk" dest="vendor/partner_gms/products/gms_minimal.mk" />
	    <copyfile src="gms_full.mk" dest="vendor/partner_gms/products/gms.mk" />
	</project>
```