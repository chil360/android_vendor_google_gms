#!/bin/bash

copyFiles()
{
    FILES="$(find temp/$1/ -mindepth 2 -type f -printf '%P\n')"
    for SRC in $FILES; do
      DEST="${SRC//___//}"
      mkdir -p "$(dirname proprietary/product$DEST)"
      cp temp/$1/$SRC proprietary/product$DEST
    done
}

## Unzip the NikGapps Core Package
unzip -d temp $1

## Extra Files
unzip -d temp/ExtraFiles temp/AppSet/Core/ExtraFiles.dat.br
copyFiles ExtraFiles
rm -r proprietary/product/lib64/
rm proprietary/product/etc/sysconfig/preinstalled-packages-platform-overlays.xml
rm proprietary/product/etc/sysconfig/preinstalled-packages-platform-handheld-product.xml

## GmsCore
unzip -d temp/GmsCore temp/AppSet/Core/GmsCore.dat.br
copyFiles GmsCore
rm -r proprietary/product/priv-app/PrebuiltGmsCore/m/

## GoogleCalendarSyncAdapter
unzip -d temp/GoogleCalendarSyncAdapter temp/AppSet/Core/GoogleCalendarSyncAdapter.dat.br
copyFiles GoogleCalendarSyncAdapter

## GoogleContactsSyncAdapter
unzip -d temp/GoogleContactsSyncAdapter temp/AppSet/Core/GoogleContactsSyncAdapter.dat.br
copyFiles GoogleContactsSyncAdapter

## GooglePlayStore
unzip -d temp/GooglePlayStore temp/AppSet/Core/GooglePlayStore.dat.br
copyFiles GooglePlayStore
rm -r proprietary/product/priv-app/Phonesky/lib/

## GoogleServicesFramework
unzip -d temp/GoogleServicesFramework temp/AppSet/Core/GoogleServicesFramework.dat.br
copyFiles GoogleServicesFramework

## Cleanup
rm -r temp/